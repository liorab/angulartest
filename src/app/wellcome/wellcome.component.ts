import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-wellcome',
  templateUrl: './wellcome.component.html',
  styleUrls: ['./wellcome.component.css']
})
export class WellcomeComponent implements OnInit {

  constructor(public authService:AuthService) { }
email:string;
  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.email = user.email;
        })
  }

}
