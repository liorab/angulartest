import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Posts } from '../interfaces/posts';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {

  Posts$:Observable<any>;
  userId:string;
  likes = 0;
  constructor(private postsService:PostsService,public authService:AuthService) { }

 ngOnInit() {
   
   
  this.authService.user.subscribe(
    user => {
      this.userId = user.uid;
      this.Posts$ = this.postsService.getSavedPosts(this.userId);
    }
  )
}
addLikes(postId:string){
  this.likes++;
  this.postsService.updateLikes(this.userId,postId,this.likes);
  
}
deletePost(PostId:string){
  this.postsService.deletePost( this.userId,PostId);
}


  }


