import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Posts } from '../interfaces/posts';
import { ActivatedRoute, Router } from '@angular/router';
import { Comments } from '../interfaces/comments';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  Posts$:Posts[];
  Comments$:Comments[];
  SavedPosts$:Observable<any>;
  userId:string;
  constructor(private postsService:PostsService,public authService:AuthService,private route:ActivatedRoute,private router:Router) { }

 ngOnInit() {

   this.authService.user.subscribe(
    user => {
      this.userId = user.uid;
     }
     
  )
  
 
  return (this.postsService.getPosts().subscribe(data =>this.Posts$ = data ),
  this.postsService.getComments().subscribe(data =>this.Comments$ = data)); 
  }
  savePost(post:object[]){
  
    this.postsService.savePost( this.userId,post);
    
  }
  
  

}
