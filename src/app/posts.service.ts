import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Posts } from './interfaces/posts';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Comments } from './interfaces/comments';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  posturl="https://jsonplaceholder.typicode.com/posts/";
  commenturl="http://jsonplaceholder.typicode.com/comments"

  constructor(private http:HttpClient,private db:AngularFirestore,public router:Router) { }
  postCollection:AngularFirestoreCollection=this.db.collection('posts');
  userCollection:AngularFirestoreCollection=this.db.collection('users');
  
  getPosts(){
    return this.http.get<Posts[]>(this.posturl);
  }

    getComments():Observable<any>{
      return this.http.get<Comment[]>(this.commenturl);
    }
    savePost(userId:string,post:object[]){
  
      this.userCollection.doc(userId).collection('posts').add(post);
    }
    getSavedPosts(userId:string):Observable<any[]>{
      this.postCollection= this.db.collection(`users/${userId}/posts`);
      return this.postCollection.snapshotChanges().pipe(
        map(
          collection => collection.map(
            document => {
              const data = document.payload.doc.data();
              data.id= document.payload.doc.id;
              console.log(data);
              return data;
            }
          )
        )
      )
      
    }
    deletePost(userId:string,postId:string){
      this.db.doc(`users/${userId}/posts/${postId}`).delete();
    }
    updateLikes(userId:string,postId:string,likes:number){
      const like=likes;
      this.db.doc(`users/${userId}/posts/${postId}`).set(likes);
     
    }
   
}
