// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDCu59sKonavfb0NX5yNtJJqiL3z_XyUrQ",
    authDomain: "angulartest-74771.firebaseapp.com",
    databaseURL: "https://angulartest-74771.firebaseio.com",
    projectId: "angulartest-74771",
    storageBucket: "angulartest-74771.appspot.com",
    messagingSenderId: "825383012330",
    appId: "1:825383012330:web:36b87dd4d7201520ff9493",
    measurementId: "G-W81DV77MSS"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
